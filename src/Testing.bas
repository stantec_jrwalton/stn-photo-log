Attribute VB_Name = "Testing"
Option Explicit
Sub TemplateTest()

    Dim template As String
    'template = GetTemplate()
    template = "jof_[date: yyyy-mmm-dd]"
    
    Dim templateDate As Date
    templateDate = Now
    
    Dim dateRegExp As New RegExp
    dateRegExp.pattern = "\[date:\s*([\s\\/_\-,ymd]*)\]"
    dateRegExp.Global = True
    dateRegExp.IgnoreCase = False
    
    Dim dateFormat, dateString As String
    Dim dateFormatMatch As Match
    
    dateString = ""
    For Each dateFormatMatch In dateRegExp.Execute(template)
        dateFormat = dateFormatMatch.SubMatches.Item(0)
        template = Replace(template, dateFormatMatch.Value, Format(templateDate, dateFormat))
    Next dateFormatMatch
    
    MsgBox template

End Sub

Function GetTemplate(Optional prompt As String = "Name Template: ") As String
    GetTemplate = InputBox(prompt)
End Function

Sub IBTest()

    Dim promptString As String: promptString = Join(Array("Hello", "My", "Name"), vbCrLf)
    MsgBox InputBox(promptString)

End Sub
