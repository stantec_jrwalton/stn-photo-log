Attribute VB_Name = "Setup"
Option Compare Text 'treat "J" and "j" as the same

Dim fso As New FileSystemObject

Private Sub AddReferences()
    Application.VBE.ActiveVBProject.References.AddFromFile ("C:\Windows\SysWOW64\scrrun.dll")
    Application.VBE.ActiveVBProject.References.AddFromFile ("C:\Windows\SysWOW64\vbscript.dll\3")
    Application.VBE.ActiveVBProject.References.AddFromFile ("C:\Program Files (x86)\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.OLB")
End Sub

Private Sub ImportModules()
    Dim app As VBProject
    Set app = Application.VBE.ActiveVBProject
    Dim moduleFile As File
    For Each moduleFile In GetSourceFiles()
        AddModule moduleFile
    Next moduleFile
End Sub

Private Sub AddModule(moduleFile As File, Optional replaceModule As Boolean = True)
    Dim component As VBComponent, componentCollection As VBComponents
    Set componentCollection = Application.VBE.ActiveVBProject.VBComponents
    Select Case Left(moduleFile.name, 5)
        Case "ThisD"
            InsertDocumentCode moduleFile.path
        Case "Setup"
            Debug.Print "Setup Utilities"
        Case Else
            Debug.Print fso.GetBaseName(moduleFile.path)
            Set component = GetComponent(fso.GetBaseName(moduleFile.path))
            If component Is Nothing Then
                Set component = componentCollection.Import(moduleFile.path)
            Else
                If replaceModule = True Then
                    componentCollection.Remove component
                    Set component = componentCollection.Import(moduleFile.path)
                Else
                    Debug.Print "Module exists and will not replace"
                End If
            End If
    End Select
End Sub

Private Function GetComponent(componentName As String) As VBComponent
    Set GetComponent = Nothing
    
    Dim comp As VBComponent
    For Each comp In Application.VBE.ActiveVBProject.VBComponents
        If comp.name = componentName Then
            Set GetComponent = comp
        End If
    Next comp
End Function

Private Sub ExportModules()
    Dim app As VBProject, comp As VBComponent, compExt As String, compFileName As String
    Set app = Application.VBE.ActiveVBProject
    
    For Each comp In app.VBComponents
        Select Case comp.Type
            Case vbext_ct_ClassModule, vbext_ct_Document
                compExt = ".cls"
            Case vbext_ct_StdModule
                compExt = ".bas"
            Case vbext_ct_MSForm
                compExt = ".frm"
            Case Else
                Debug.Print "Module Type not recognized"
                compExt = ""
        End Select
        
        ' Don't export vendor modules to './src'
        If Left(comp.name, 3) = "GPS" Then
            Debug.Print "Skip exporting: " & comp.name
        Else
            comp.Export ResolvePath("../src", comp.name & compExt)
        End If
    Next comp
End Sub

Private Sub RemoveModules()
    Dim app As VBProject, comp As VBComponent
    Set app = Application.VBE.ActiveVBProject
    For Each comp In app.VBComponents
        Debug.Print comp.name
        If comp.Type = vbext_ct_Document Then
            'comp.CodeModule.DeleteLines 1, comp.CodeModule.CountOfLines
        Else
            'app.VBComponents.Remove comp
        End If
    Next comp
End Sub

Private Sub InsertDocumentCode(filePath As String)
    Dim comp As VBComponent, thisDoc As VBComponent, componentCollection As VBComponents
    Set comp = Application.VBE.ActiveVBProject.VBComponents.Import(filePath)
    Set componentCollection = Application.VBE.ActiveVBProject.VBComponents
    Set thisDoc = componentCollection.Item(1)
    
    ' Clear the Code Module
    thisDoc.CodeModule.DeleteLines 1, thisDoc.CodeModule.CountOfLines
    
    ' Insert new code from source file by importing file and then deleting it
    ' (if you add text it will bring in the "VB_Attribute" properties as well)
    thisDoc.CodeModule.InsertLines 1, comp.CodeModule.Lines(1, comp.CodeModule.CountOfLines)
    componentCollection.Remove comp
End Sub

Private Function ResolvePath(ParamArray paths() As Variant) As String
    Dim path As String
    path = ""
    If Left(paths(0), 1) = "." Then
        path = ActiveDocument.path
    End If
    Dim i As Long
    For i = 0 To UBound(paths)
        path = fso.BuildPath(path, paths(i))
    Next i
    ResolvePath = fso.GetAbsolutePathName(path)
End Function

Private Function GetSourceFiles(Optional sourceType As String = "All") As Collection
    Dim fileCollection As New Collection
    Dim moduleFile As File
    
    If sourceType = "Source" Or sourceType = "All" Then
        For Each moduleFile In fso.GetFolder(ResolvePath("../src")).Files
            fileCollection.Add moduleFile
        Next moduleFile
    End If
    
    If sourceType = "Vendor" Or sourceType = "All" Then
        For Each moduleFile In fso.GetFolder(ResolvePath("../vendor")).Files
            fileCollection.Add moduleFile
        Next moduleFile
    End If
    
    Set GetSourceFiles = fileCollection
End Function





