Attribute VB_Name = "RenamePhotos"
Option Explicit
Option Compare Text

Private fso As New FileSystemObject
Private templateRegExp As New RegExp

Private Sub RenamePhotos()

' This should be the main entry point, from which
' "CompilePhotos" and "CopyPhotos" are called with arguments

End Sub

Sub CompilePhotos()
    
    Dim result As Object
    
    Dim srcPath As String
    Set result = GetFolder("Select Source Folder")
    If result Is Nothing Then Exit Sub
    srcPath = result.Item(1)
    Set result = Nothing
    'srcPath = ActiveDocument.Path & "\tests"
    
    Dim destPath As String
    Set result = GetFolder("Select Destination Folder")
    If result Is Nothing Then Exit Sub
    destPath = result.Item(1)
    Set result = Nothing
    'destPath = ActiveDocument.Path
    
    Dim baseName As String
    baseName = InputBox("Base Name for photos: (i.e. basename_20160505_01)")
    If baseName = "" Then Exit Sub
    
    Dim recursive As Boolean
    Dim recursiveResult As VbMsgBoxResult
    recursiveResult = MsgBox("Search Sub-Folders for Photos?", vbYesNoCancel)
    If recursiveResult = vbCancel Then Exit Sub
    
    If recursiveResult = vbYes Then
        recursive = True
    Else
        recursive = False
    End If
    
    Dim folderQueue As Collection
    Set folderQueue = New Collection
    
    folderQueue.Add srcPath
    
    Dim photoDict As Scripting.Dictionary
    Set photoDict = New Scripting.Dictionary
    StatusBar = "Processing Photos..."
    Do While folderQueue.Count() > 0
        Dim folderPath As String
        folderPath = folderQueue.Item(1)
        folderQueue.Remove (1)
        Dim photoDir As folder
        Set photoDir = fso.GetFolder(folderPath)
        
        If recursive Then
            Dim folder As folder
            For Each folder In photoDir.subfolders
                folderQueue.Add folder
            Next folder
        End If
        
        Dim photo As File
        For Each photo In photoDir.Files
            If photo Like "*.jpg" Then
                Dim dateTaken As Date
                dateTaken = ReadDate(photo.Path)
                Dim dateTakenString As String
                dateTakenString = Format(dateTaken, "yyyymmdd")
                
                Dim dateCol As Collection
                If photoDict.Exists(dateTakenString) Then
                    Set dateCol = photoDict.Item(dateTakenString)
                Else
                    Set dateCol = New Collection
                    photoDict.Add dateTakenString, dateCol
                End If
                dateCol.Add photo
            End If
        Next
    Loop
    StatusBar = "Completed Processing Photos"
    Call CopyPhotos(destPath, baseName, photoDict)
    StatusBar = "Completed Copying Photos"
    MsgBox "Completed Compiling Photos", vbOKOnly
End Sub

Private Sub CopyPhotos(rootDestPath As String, baseName As String, ByRef photoDict As Scripting.Dictionary)
    'Perform the copy operation after photos have been parsed and added to photo dictionary (list of photos)
    
    Dim key As Variant
    Dim photo As File
    Dim destPath As String
    Dim destFilePath As String
    Dim counter As Integer
    
    For Each key In photoDict.Keys
        'key will represent the photo date taken in YYYYMMDD format
        Dim dateTaken As String
        dateTaken = CStr(key)
        StatusBar = "Copying Photos to " & dateTaken
        
        destPath = fso.BuildPath(rootDestPath, dateTaken)
        'if the folder already exists, we delete it
        'to clear the way for new photos
        If fso.FolderExists(destPath) Then
            'folder already exists from previous operation
            fso.DeleteFolder destPath, True
        End If
        're-create folder
        fso.CreateFolder (destPath)
        
        counter = 1
        For Each photo In photoDict.Item(dateTaken)
            destFilePath = fso.BuildPath(destPath, BuildPhotoName(baseName, dateTaken, counter))
            fso.CopyFile photo.Path, destFilePath
            counter = counter + 1
        Next photo
    Next key
End Sub

Function BuildPhotoName(baseName As String, dateTaken As String, counter As Integer, Optional sigFigs As Integer = 2) As String
    'format photo name as "baseName_YYYYMMDD_01"
    Dim counterString As String
    counterString = Format(counter, String(sigFigs, "0"))
    BuildPhotoName = Join(Array(baseName, dateTaken, counterString), "_") & ".jpg"
    
End Function

Function BuildNameFromTemplate(template As String) As String
' not implemented yet, for future releases

'   template contains the variables:
'       c: counter
'           c   => 1, 2, ..., 15, etc.
'           cc  => 01, 02, ..., 15, etc.
'           ccc => 001, 002, ..., 015, ..., 105, etc.
'
'       y: year
'           yy  => 01, 02, ..., 12
'           yyyy=> 1999, 2000, ..., 2016
'
'       m: month
'           m   => 1, 2, ..., 12
'           mm  => 01, 02, ..., 12
'           mmm => Jan, Feb, ..., Dec
'           mmmm=> January, February, ..., December
'
'       d: day
'           d   => 1, 2, ..., 31
'           dd  => 01, 02, ..., 31
'           ddd => Mon, Tue, ..., Sun
'           dddd=> Monday, Tuesday, ..., Sunday
'
'   Ex:
'       template:   jof_<yyyymmdd>_<ccc>
'       result:     jof_20160606_002.jpg
    
    templateRegExp.pattern = "<"
    
End Function

Function GetFolder(prompt As String, Optional multiple As Boolean = False) As Variant
    With Application.FileDialog(msoFileDialogFolderPicker)
        .Title = prompt
        .AllowMultiSelect = multiple
        .InitialFileName = ActiveDocument.Path ' & "\"
        If .Show <> -1 Then
            Set GetFolder = Nothing
        Else
            Set GetFolder = .SelectedItems
        End If
    End With
End Function

Function ReadDate(photoPath As String) As Date
    Dim dateTimeString As String
    dateTimeString = GPSExifReader.OpenFile(photoPath).DateTimeOriginal
    If dateTimeString = "" Then
        ReadDate = fso.GetFile(photoPath).DateLastModified
    Else
        Dim result As SubMatches
        Set result = ExecRegExp("(\d{4}):(\d{2}):(\d{2})", dateTimeString).Item(0).SubMatches
        Dim dateArray() As Variant
        dateArray = Array(result.Item(1), result.Item(2), result.Item(0)) 'month, day, year
        ReadDate = CDate(Join(dateArray, "/"))
    End If
End Function

Function ReadDateFromFile(photoFile As File) As Date

    ReadDateFromFile = ReadDate(photoFile.Path)

End Function

Private Sub ReadDateTest()
    Dim filePath As String
    filePath = "C:\Users\jrwalton\Pictures\2015_12_10-jof\IMG_5884.JPG"
    MsgBox ReadDateFromFile(fso.GetFile(filePath))
End Sub

Function DateToString() As String

End Function

Function ExecRegExp(pattern As String, src As String) As MatchCollection
    Dim re As RegExp
    Set re = New RegExp
    re.pattern = pattern
    re.Global = True
    re.IgnoreCase = True
    
    Dim res As MatchCollection
    Set res = re.Execute(src)

    Set ExecRegExp = res
    Set res = Nothing
End Function

Private Sub RegExpTest()
    Dim res As Date
    res = ReadDate("Z:\Software\Utilities\photo-organizer\v3.0.1\tests\008.JPG")
End Sub

Private Sub BuildTest()
    Dim name As String
    name = BuildPhotoName("test", "20150202", 5, 3)
End Sub
