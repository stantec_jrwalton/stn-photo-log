Attribute VB_Name = "GenerateLog"
Option Explicit 'Must use "Dim" before variable is used
Option Compare Text 'Treat "J" and "j" as the same

Sub GeneratePhotoLog()
    Call ClearTemplate

    Dim oFolder As folder, _
        oSubfolder As folder, _
        oFile As File, _
        queue As Collection
    Dim savePath As String
    Dim fso As New FileSystemObject
    Set queue = New Collection
    Dim xRow As Long, xCol As Long, cntr As Long
    cntr = 1
    xRow = 1
    xCol = 1
  
    With Application.FileDialog(msoFileDialogFolderPicker)
        .Title = "Select Picture Folder"
        .AllowMultiSelect = False
        If .Show <> -1 Then
        Exit Sub
        Else
            queue.Add fso.GetFolder(.SelectedItems(1))
        End If
    End With
  
    With Application.FileDialog(msoFileDialogFolderPicker)
        .Title = "Select Destination Folder"
        .AllowMultiSelect = False
        If .Show <> -1 Then
            Exit Sub
        Else
            savePath = .SelectedItems(1)
        End If
    End With
    
    StatusBar = "Collecting Photos..."
    
    Dim cell_height As Double, cell_width As Double, ratio As Double, scaleFactor As Double
    
    Dim current_cell As cell
    Dim pic As Word.InlineShape
    
    Dim destFilePath As String
    
    Application.ScreenUpdating = False
    Do While queue.Count > 0
        Set oFolder = queue(1)
        queue.Remove 1
 
        With ActiveDocument.Tables(2)
        
            'Keep the columns from updating width when large photos are added
            'that don't fit inside the cell
            '(the photo will be resized to fit the cell after it is added)
            .AllowAutoFit = False
        
            For Each oSubfolder In oFolder.subfolders
                queue.Add oSubfolder
            Next oSubfolder
            
            For Each oFile In oFolder.Files
                'currently only processes JPEG files, will add in future versions
                If oFile Like "*.jpg" Then
                    StatusBar = "Adding photo: " & fso.GetBaseName(oFile.Path)
                    Set current_cell = .cell(Row:=xRow, Column:=xCol)
                    
                    Set pic = current_cell.Range.InlineShapes.AddPicture(FileName:=oFile)
                    
                    ' Get the scale ratio to shrink the file to fit the cell.
                    ratio = pic.Width / pic.Height
                    scaleFactor = Min(current_cell.Height / pic.Height, current_cell.Width / pic.Width)
                    
                    pic.Height = pic.Height * scaleFactor * 0.98 'give it some vertical padding
                    pic.Width = pic.Height * ratio
                    
                    xRow = xRow + 1
                    .cell(Row:=xRow, Column:=xCol).Range.InsertAfter Text:=oFile.name
                    If xCol < .Columns.Count Then
                        xCol = xCol + 2
                        xRow = xRow - 1
                    Else
                        xCol = 1
                        xRow = xRow + 1
                    End If
                    If xRow > .Rows.Count Then
                        ' v3.1.2 - use FileSystemObject.BuildPath to fix error
                        ' when output path has spaces (pdf export fails).
                        destFilePath = fso.BuildPath(savePath, "thumbs_" & cntr & ".pdf")
                        
                        StatusBar = "Exporting: " & destFilePath
                        ExportPDF (destFilePath)
                        cntr = cntr + 1
                        Call ClearTemplate
                        xRow = 1
                        xCol = 1
                    End If
                End If
            Next oFile
        End With
    Loop
    
    destFilePath = fso.BuildPath(savePath, "thumbs_" & cntr & ".pdf")
    StatusBar = "Exporting: " & destFilePath
    ExportPDF (destFilePath)
    
    Call ClearTemplate
    
    Application.ScreenUpdating = True
    StatusBar = "Completed Exporting Photolog"
    MsgBox "Completed Exporting Photo Log"
    StatusBar = "" 'reset status bar
End Sub

Private Sub ExportPDF(filePath As String)
    ActiveDocument.ExportAsFixedFormat _
    OutputFileName:=filePath, _
    ExportFormat:=wdExportFormatPDF, _
    OpenAfterExport:=False, _
    Range:=wdExportFromTo, From:=2, To:=2
End Sub

Private Sub DeleteEmptyRows()

    Application.ScreenUpdating = False
    
    Dim Tbl As Table, cel As cell, i As Long, n As Long, fEmpty As Boolean
    
    With ActiveDocument
        For Each Tbl In .Tables
            n = Tbl.Rows.Count - 2
            For i = n To 1 Step -1
                fEmpty = True
                For Each cel In Tbl.Rows(i).Cells
                    If Len(cel.Range.Text) > 2 Then
                        fEmpty = False
                        Exit For
                    End If
                Next cel
                If fEmpty = True Then Tbl.Rows(i).Delete
            Next i
            If fEmpty = True Then Tbl.Delete
        Next Tbl
    End With
    
    Set cel = Nothing
    Set Tbl = Nothing
    
    Application.ScreenUpdating = True

End Sub

Sub ClearTemplate()
    ActiveDocument.Tables(2).Range.Delete
End Sub

Private Sub SelectTable()
    ActiveDocument.Tables(2).Select
    'ActiveDocument.Tables(2).Range.Delete
End Sub

Private Function Min(var1 As Double, var2 As Double) As Double
    If var1 > var2 Then
        Min = var2
    Else: Min = var1
    End If
End Function

Private Sub ReportCellInfo()
    Dim selected_cell As cell
    Set selected_cell = ActiveDocument.Tables(2).cell(1, 3)
    Debug.Print selected_cell.RowIndex & ", " & selected_cell.ColumnIndex & _
    " - Width: " & selected_cell.Width & ", Height: " & selected_cell.Height
End Sub

Private Sub Fit()
    Selection.Columns(1).AutoFit
End Sub
