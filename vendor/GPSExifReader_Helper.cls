VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "GPSExifReader_Helper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' ********************************************************************************
' *  Virtual-COM library: GPSExifReader_Helper                                   *
' *  v1.0.0                      built Aug 27 2011                               *
' * - Usage guide: http://www.everythingaccess.com/exifgps                       *
' * ---------------------------------------------------------------------------- *
' * - Written by Wayne Phillips (EverythingAccess.com)                           *
' * - brought to you by Australia's North Central Catchment Management Authority *
' * - (www.nccma.vic.gov.au)                                                     *
' * ---------------------------------------------------------------------------- *
' * - v1.0  28/08/2011 - Initial release.                                        *
' ********************************************************************************

Option Explicit

Public New_vbExifProperties As New GPSExifProperties
