# Stantec Photo Log
## Utility application for generating photo logs from pictures taken during field visits / construction observation

# Building the application
* Open public/stn-photo-log.xml
* Add References (currently Microsoft Scripting Runtime and Microsoft RegExp 5.5)
* Insert Modules from src/ and vendor/
* Save Macros from ThisDocument.cls into ThisDocument (macros to control buttons)
* Save the file as *.docm in the working/ or dist/ folder

