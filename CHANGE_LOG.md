# Change log

## v3.1.2
* Fixed bug when writing pdf's to a folder path containing spaces
* Adding more progress reporting as well as a MsgBox when complete

## v3.1.3
* Fix bug where pdf's have a low image quality